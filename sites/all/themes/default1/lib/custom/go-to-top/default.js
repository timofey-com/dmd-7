$(document).ready(function () {

    // Go To Top button
    // TODO: Inject HTML of the button

    // animate go to top
    $("a[href='#top']").click(function () {
        $('html,body').animate({
            scrollTop: 0
        }, 1000);
        return false;
    });

    // show/hides Go To Top button
    $(window).scroll(function () {
        if ($(this).scrollTop()) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });

});