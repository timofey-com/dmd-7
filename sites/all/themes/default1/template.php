<?php

// README
// Change all instances marked with "CHANGEME"

/**
 * Implements hook_html_head_alter().
 */
function default1_html_head_alter(&$head_elements) {
  // Remove unnecessary meta tags set by Drupal
  // 1) By default, Drupal adds a meta tag for HTML4, called 'system_meta_content_type'. Get rid of it and in hook_preprocess_html add necessary meta tags for HTML5.
  unset($head_elements['system_meta_content_type']);
  // 2) Drupal preloads adds an unnecessary tag called 'system_meta_generator', specifying cms type. Get rid of it.
  unset($head_elements['system_meta_generator']);
}

/**
 * Override or insert variables into the html template.
 */

// TODO: Move what you can to custom_pages module
function default1_preprocess_html(&$vars) {
  // Get additional variables
  $meta_description = variable_get('meta_description');

  // Get path to theme. Needed for favicon
  $path_to_theme = base_path() . path_to_theme();

  // Get existing head elements
  $head = drupal_add_html_head();

  // Remove title suffix from front page, e.g. remove '| Site Name'
  if ($vars['is_front']) {
    $vars['head_title'] = $vars['head_title_array']['title'];
  }

  // Add meta tags to head
  $elements = array();

  // UTF-8
  $elements['charset'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'charset' => 'UTF-8',
    )
  );
  // Viewport
  $elements['viewport'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1.0',
    )
  );
  // For IE, render in latest standards, @see http://stackoverflow.com/questions/6771258
  // Note: chrome=1 removed since chrome frame is no longer supported, @see https://github.com/h5bp/html5-boilerplate/pull/1396
  $elements['x-ua-compatible'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' => 'IE=edge',
    )
  );
  $elements['description'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'description',
      'content' => isset($head['description']['#attributes']['content']) ? $head['description']['#attributes']['content'] : $meta_description,
    )
  );
  // Open Graph
  // For locale list see https://www.facebook.com/translations/FacebookLocales.xml
  $elements['og:title'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:title',
      'content' => $vars['head_title'],
    )
  );
  $elements['og:type'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:type',
      'content' => 'website',
    )
  );
  $elements['og:image'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:image',
      'content' => isset($head['og:image']['#attributes']['content']) ? $head['og:image']['#attributes']['content'] : $path_to_theme . '/img/og:image.png',
    )
  );
  $elements['og:description'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:description',
      'content' => $elements['description']['#attributes']['content'],
    )
  );
  $elements['og:site_name'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:site_name',
      'content' => $vars['head_title_array']['name'],
    )
  );
  // Twitter Card
  $elements['twitter:card'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'twitter:card',
      'content' => 'summary_large_image',
    )
  );
  $elements['twitter:site'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'twitter:site',
      'content' => '@CHANGEME',
    )
  );
  $elements['twitter:title'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'twitter:title',
      'content' => $vars['head_title'],
    )
  );
  // Twitter fallback
  // Twitter doesn't need additional tags, since it fallsback on OG
  // twitter:description => og:description
  // twitter:title => og:title
  // twitter:image => og:image

  $elements['twitter:creator'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'twitter:creator',
      'content' => '@CHANGEME',
    )
  );

  foreach ($elements as $key => $element) {
    drupal_add_html_head($element, $key);
  }

  // Add link tags to head
  $elements = array(
    // Google Font
    array(
      'href' => 'http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic',
      'rel' => 'stylesheet',
      'type' => 'text/css',
    ),
    array(
      'href' => 'http://fonts.googleapis.com/css?family=Oswald:400,300,700',
      'rel' => 'stylesheet',
      'type' => 'text/css',
    ),
    array(
      'href' => $path_to_theme . '/lib/contributed/bower_components/font-awesome/css/font-awesome.min.css',
      'rel' => 'stylesheet',
      'type' => 'text/css',
    ),
    // Google+  confirmation link
//    array(
//      'href' => 'https://plus.google.com/+CHANGEME',
//      'rel' => 'publisher',
//    ),
  );

  foreach ($elements as $element) {
    drupal_add_html_head_link($element);
  }

  // Custom variable - $browser_message
  // Adds browser-specific messages, i.e. browser not supported
  $vars['browser_message'] = '<!--[if lte IE 8]><div class="ie-specific" style="border-bottom: 2px solid #000; padding: 5% 20%; font-size: 1.5em; text-align: center;"><p>This site doesn\'t support Internet Explorer 8 or below. But come on, your browser really needs an upgrade! <br>We recommend <a style="border-bottom: solid 1px #000;" href="https://www.google.com/intl/en/chrome/browser/">Google Chrome</a> or <a style="border-bottom: solid 1px #000;" href="http://www.mozilla.org/en-US/firefox/new/">Firefox</a>, but if you still want, you can download a newer <a style="border-bottom: solid 1px #000;" href="http://windows.microsoft.com/en-US/internet-explorer/downloads/ie">Microsoft Internet Explorer</a>.</p></div><![endif]-->';

}